from Bataille.Partie import Partie
from Bataille.TourBatailleCourte import TourBatailleCourte


class BatailleCourte(Partie):
    def __init__(self, log, plateau, joueurs, cartecachee):
        super().__init__(log, plateau, joueurs, cartecachee=cartecachee)
        self.tour = TourBatailleCourte(log, plateau, self.joueurs, cartecachee=cartecachee, indexjoueurfenetre=0)

