from Bataille.Partie import Partie
from Bataille.TourBatailleLongue import TourBatailleLongue


class BatailleLongue(Partie):
    def __init__(self, log, plateau, joueurs, cartecachee):
        super().__init__(log, plateau, joueurs, cartecachee=cartecachee)
        self.tour = TourBatailleLongue(log, plateau, self.joueurs, cartecachee=cartecachee, indexjoueurfenetre=0)


