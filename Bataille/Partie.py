from Bataille.Tour import Tour
from Jeu.JoueurFenetre import JoueurFenetre
from Deck.Deck import Deck
from Observer.Subject import Subject


class Partie(Subject):
    def __init__(self, log, plateau, joueurs, cartecachee):
        super().__init__()
        self.joueurs = joueurs
        self.cartecachee = cartecachee
        self.deck = Deck("Melangé")
        #self.distribue()
        self.tour = None
        self._indexJoueurFenetre = 0
        self.log = log
        self.plateau = plateau
        self.attach(self.log)
        self.attach(self.plateau)

    def sort_joueurs(self):
        self.joueurstries = [j.index for j in self.joueurs]
        self.joueurs = self.joueurstries

    #distribution des cartes aux joueurs
    def distribue(self):
        nb_joueurs = len(self.joueurs)
        if nb_joueurs == 3:
            self.deck.pioche()
        nb_cartes_par_paquet = len(self.deck.pile) // nb_joueurs
        for joueur in self.joueurs:
            i = 0
            while i < nb_cartes_par_paquet:
                joueur.insererCarte(self.deck.pioche())
                i += 1

    def afficher_score_joueurs(self):
        for joueur in self.joueurs:
            self.subject_state = "Score de " + joueur.pseudo+" : "+str(joueur.score_defausse())

    #lance un tour
    def jouer(self):
        self.subject_state = "Vous êtes " + self.getJoueurFenetre().pseudo
        while not self.checkEmptyDecks():
            self.tour.bataille(self.tour.joueurs)

    def checkEmptyDecks(self):
        if len(self.joueurs) == 0:
            for joueur in self.joueurs:
                if len(joueur.deck.pile) == 0:
                    self.subject_state = joueur.pseudo + " a un deck vide !"
                    self.subject_state = joueur.pseudo + " a perdu !"
                    self.subject_state = "Taille du deck : " + str(len(joueur.deck.pile))
                    self.subject_state = "Taille du defausse : " + str(len(joueur.defausse.pile))
                    self.elimineJoueurs()
                    return True
        return False

    def elimineJoueurs(self):
        joueurs_a_eliminer = [joueur for joueur in self.joueurs if not joueur.deck]
        for joueur in joueurs_a_eliminer:
            self.joueurs.remove(joueur)

    def ajouterJoueur(self, joueur):
        if len(self.joueurs) > 4:
            self.joueurs.append(joueur)

    def stop(self):
        self.tour.stop()

    def initTour(self):
        self.tour = Tour(self.log, self.plateau, self.joueurs, self.cartecachee, self._indexJoueurFenetre)

    def getJoueurByIndex(self, index):
        return next(joueur for joueur in self.joueurs if joueur.index == index)

    def getJoueurFenetre(self):
        return self.joueurs[self._indexJoueurFenetre-1]

    @property
    def indexjoueurfenetre(self):
        return self._indexJoueurFenetre

    @indexjoueurfenetre.setter
    def indexjoueurfenetre(self, value):
        self._indexJoueurFenetre = value
        self.tour.indexjoueurfenetre = value

