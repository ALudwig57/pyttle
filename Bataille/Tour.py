import abc
import time

from Errors.BatailleError import BatailleError
from Observer.Subject import Subject
from Jeu.JoueurFenetre import JoueurFenetre


def getrang(item):
    if item[1] is None:
        raise BatailleError("Le joueur n'a pas de carte actuelle", "No card")
    return item[1].rang


def getjoueur(item):
    return item[0]


def check_equal(cartejouees):
    bestCards = sorted(cartejouees, key=getrang, reverse=True)
    return [item for item in bestCards if item[1] == bestCards[0][1]]


class Tour(Subject):
    __metaclass__ = abc.ABCMeta

    def __init__(self, log, plateau, joueurs, cartecachee, indexjoueurfenetre):
        Subject.__init__(self)
        self.joueurs = joueurs
        self.cartecachee = cartecachee
        self.indexjoueurfenetre = indexjoueurfenetre
        self.attach(log)
        self.attach(plateau)
        # Carte de type(nom du joueur qui a posé la carte ; carte
        self.cartes_jouees = list()
        # Carte de type(nom du joueur qui a posé la carte ; carte
        self.cartes_gagnant = list()
        self.cartes_batailles = list()

    @abc.abstractmethod
    def donneCartesAuGagnant(self, cartesGagnantes, listofcards, cartes_batailles):
        return

    def bataille(self, joueurs):
        # for joueur in joueurs:
        #     self.subject_state = str(joueur.Index) + "| " + joueur.pseudo + " a joué un |None"
        if len(joueurs) < 2:
            self.subject_state = joueurs[0].pseudo + "a gagné."
            return
        for joueur in joueurs:
            print("Au tour de "+joueur.pseudo)
            try:
                joueur.jouer()
            except BatailleError:
                joueurs.remove(joueur)
                break
            self.cartes_jouees.append((joueur, joueur.CarteActuelle))
            self.subject_state = joueur.pseudo + " a encore " + str(len(joueur.Deck.pile)) + " cartes"
            self.subject_state = str(self.decalageindex(index=joueur.Index, indexjoueurfenetre=self.indexjoueurfenetre)) + "|" + joueur.pseudo + " a joué |" + str(joueur.CarteActuelle)

        cartesGagnantes = check_equal(self.cartes_jouees)
        listofcards = [i[1] for i in self.cartes_jouees]
        time.sleep(5)
        if len(cartesGagnantes) == 1:
            self.subject_state = cartesGagnantes[0][0].pseudo + " a gagné"
            self.donneCartesAuGagnant(cartesGagnantes, listofcards, self.cartes_batailles)
        else:
            self.egalite([item[0] for item in cartesGagnantes])
        self.cartes_jouees = list()
        for joueur in joueurs:
            joueur.CarteActuelle = None

    def egalite(self, joueurs):
        self.subject_state = "Bataille !"
        for joueur in joueurs:
            joueur.CarteActuelle = None
        if self.cartecachee:
            for joueur in joueurs:
                joueur.jouer()
                self.cartes_jouees.append((joueur, joueur.CarteActuelle))
                self.subject_state = joueur.pseudo + " a encore " + str(len(joueur.Deck.pile)) + " cartes"
                self.subject_state = str(self.decalageindex(index=joueur.Index, indexjoueurfenetre=self.indexjoueurfenetre)) + "|" + joueur.pseudo + " a joué |back"

        while len(self.cartes_jouees) > 0:
            self.cartes_batailles.append(self.cartes_jouees.pop(0)[1])
        self.bataille(joueurs)

    def gagnant(self, joueurs):
        carte_gagnante = self.cartes_jouees.pop(0)
        for joueur in joueurs:
            if carte_gagnante == joueur.CarteActuelle:
                return joueur
        raise BatailleError("Gagnant", "Pas de gagnant")

    def decalageindex(self, index, indexjoueurfenetre):
        if index == 0:
            raise Exception("NUL NUL NUL")
        indexfinal = index - (indexjoueurfenetre - 1)
        if indexfinal > 0:
            return indexfinal
        else:
           return indexfinal + len(self.joueurs)
