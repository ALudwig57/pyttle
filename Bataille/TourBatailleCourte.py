from Bataille.Tour import Tour


class TourBatailleCourte(Tour):

    def __init__(self, log, *joueurs, cartecachee, indexjoueurfenetre):
        super().__init__(log, *joueurs, cartecachee, indexjoueurfenetre)

    def donneCartesAuGagnant(self, cartesGagnantes, listofcards, cartes_batailles):
        cartesGagnantes[0][0].ajout_defausse(listofcards)
        cartesGagnantes[0][0].ajout_defausse(cartes_batailles)
        self.subject_state = cartesGagnantes[0][0].pseudo + " a un score de " + str(len(cartesGagnantes[0][0].Defausse.pile))
