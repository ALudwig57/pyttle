from Bataille.Tour import Tour


class TourBatailleLongue(Tour):

    def __init__(self, log, *joueurs, cartecachee, indexjoueurfenetre):
        super().__init__(log, *joueurs, cartecachee, indexjoueurfenetre)

    def donneCartesAuGagnant(self, cartesGagnantes, listofcards, cartes_batailles):
        cartesGagnantes[0][0].ajout_deck(listofcards)
        cartesGagnantes[0][0].ajout_deck(cartes_batailles)
