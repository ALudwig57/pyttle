from threading import Thread

from Vues.ViewConsoleReseau import ViewConsoleReseau
import abc

class Controller(Thread):
    def __init__(self):
        self.__metaclass__ = abc.ABCMeta
        super().__init__()
        self.log = None
        self.Vue = ViewConsoleReseau()

    @abc.abstractmethod
    def run(self):
        pass

    def setupPartie(self):
        self.partie = self.log.WhichPartie(*self.log.howManyPlayers())

    def tour(self, index):
        self.log.tour(index)
