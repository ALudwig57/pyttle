from Controllers.Controller import Controller


class JoueurController(Controller):

    def run(self):
        pass

    def __init__(self, joueur):
        super().__init__()
        self.joueur = joueur

    def creer_paquet(self, cartes):
        self.joueur.deck = cartes

    def AMonTour(self):
        self.Vue.tour(self.joueur)

    def afficherscore(self):
        self.Vue.afficheScore(self.joueur)

    def ajoutdeck(self, cartes):
        self.joueur.deck += cartes

    def ajoutdefausse(self, cartes):
        self.joueur.defausse += cartes

    def pioche(self):
        self.joueur.carte_actuelle = self.joueur.deck.pioche()
