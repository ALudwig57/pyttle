import threading
from Controllers.Controller import Controller


class PartieController(Controller):
    def __init__(self, app, log, plateau):
        super().__init__()
        self.partie = None
        self.joueur = None
        self.log = log
        self.app = app
        self.plateau = plateau
        self.running = True

    def run(self):
        self.setupPartie()
        self.partie.jouer()

    def setupPartie(self):
        self.partie = self.app.partie

    def tour(self, index):
        self.log.tour(index)
        self.joueur = self.Vue.setTypeOfJoueur()
        self.joueur.start()

    def tour(self, joueur):
        self.Vue.tour(joueur=joueur)
        self.partie.tour(joueur)

    # distribution des cartes aux joueurs
    def distribue(self):
        self.partie.distribue()

    def stop(self):
        threading.Thread._stop(self)

