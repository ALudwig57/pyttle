symboles = (
    "Pique",
    "Coeur",
    "Trefle",
    "Carreau"
)

rangs = (
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "Valet",
    "Dame",
    "Roi",
    "As"
)


class Carte:
    def __init__(self, symbole=0, rang=2):
        self.symbole = symbole
        self.rang = rang

    def __str__(self):
        return '%s_de_%s' % (rangs[self.rang], symboles[self.symbole])

    def __lt__(self, other):
        t1 = self.rang
        t2 = other.rang
        return t1 < t2

    def __gt__(self, other):
        t1 = self.rang
        t2 = other.rang
        return t1 > t2

    def __le__(self, other):
        t1 = self.rang
        t2 = other.rang
        return t1 <= t2

    def __ge__(self, other):
        t1 = self.rang
        t2 = other.rang
        return t1 >= t2

    def __eq__(self, other):
        t1 = self.rang
        t2 = other.rang
        return t1 == t2

    def equivaut(self, other):
        t1 = self.rang
        t2 = other.rang
        return t1 == t2
