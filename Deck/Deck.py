from random import shuffle
from Deck.Carte import *


class Deck:
    def __init__(self, type_d):
        if type_d == "Vide":
            self.pile = list()
        else:
            self.creer_deck()
        if type_d == "Melangé":
            self.melanger()

    # surcharge de l'opérateur +=
    def __iadd__(self, other):
        if other.__class__ is Carte :
            self.pile.append(other)
            return self
        elif other.__class__ is list:
            #for carte in other:
            while len(other) > 0:
                self.pile.append(other.pop(0))
            return self
        else :
            print("Erreur : l'argument n'est ni une liste de carte, ni une carte")
            return NotImplemented

    # Mélange
    def melanger(self):
        shuffle(self.pile)

    # Creaion d'un deck
    def creer_deck(self):
        self.pile = list()
        for symbole in range(4):
            for rang in range(13):
                carte = Carte(symbole, rang)
                self.pile.append(carte)

    # Pioche d'une carte
    def pioche(self):
        if not self.estVide():
            return self.pile.pop(0)

    def insererCarte(self, carte):
        self.pile.append(carte)

    def estVide(self):
        return len(self.pile) == 0
