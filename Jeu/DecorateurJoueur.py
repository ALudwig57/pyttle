import copy
from Jeu.JoueurBataille import JoueurBataille


class DecorateurJoueur(JoueurBataille):
    def __init__(self, joueur):
        super().__init__(joueur.index, joueur.pseudo)
        self.joueur = joueur

    def jouer(self):
        self.joueur.jouer()

    def creer_paquet(self, cartes):
        self.joueur.creer_paquet(cartes)

    def pioche(self):
        self.joueur.pioche()

    def ajout_deck(self, cartes):
        self.joueur.ajout_deck(cartes)

    def ajout_defausse(self, cartes):
        self.joueur.ajout_defausse(cartes)

    def score_defausse(self):
        self.joueur.score_deck()

    def tour(self, carte_jouees):
        self.joueur.tour(carte_jouees)

    @property
    def Deck(self):
        return self.joueur.Deck

    @property
    def Defausse(self):
        return self.joueur.defausse

    @property
    def doitJouer(self):
        return self.joueur.doitJouer

    @property
    def CarteActuelle(self):
        return self.joueur.CarteActuelle

    @CarteActuelle.setter
    def CarteActuelle(self, value):
        self.joueur.CarteActuelle = value

    def insererCarte(self, carte):
        self.joueur.insererCarte(carte)

    @property
    def Index(self):
        return self.joueur.index

    @Index.setter
    def Index(self, value):
        self.joueur.index = value
