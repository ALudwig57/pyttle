import abc


class Joueur:
    def __init__(self, pseudo):
        self.__metaclass__ = abc.ABCMeta
        self.pseudo = pseudo

    @abc.abstractmethod
    def jouer(self):
        pass

    @abc.abstractmethod
    def creer_paquet(self, cartes):
        pass

    @abc.abstractmethod
    def pioche(self):
        pass

    @abc.abstractmethod
    def ajout_deck(self, cartes):
        pass

    @abc.abstractmethod
    def ajout_defausse(self, cartes):
        pass

    @abc.abstractmethod
    def score_defausse(self):
        pass

    @abc.abstractmethod
    def tour(self, carte_jouees):
        pass
