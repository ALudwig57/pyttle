import time
from Deck.Deck import Deck
from Errors.BatailleError import BatailleError
from Jeu.Joueur import Joueur


class JoueurBataille(Joueur):
    def __init__(self, index, pseudo):
        super().__init__(pseudo)
        self.deck = Deck("Vide")
        self.defausse = Deck("Vide")
        self.carte_actuelle = None
        self.index = index

    def creer_paquet(self, cartes):
        self.deck = cartes

    def jouer(self):
        while self.doitJouer:
            time.sleep(0.1)

    def pioche(self):
        if self.deck.estVide():
            raise BatailleError("Perdu", "Plus de cartes")
        self.carte_actuelle = self.deck.pioche()

    def insererCarte(self, carte):
        self.deck.insererCarte(carte)

    def ajout_deck(self, cartes):
        self.deck += cartes

    def ajout_defausse(self, cartes):
        self.defausse += cartes

    def score_defausse(self):
        score = 0
        for carte in self.defausse.pile:
            score += carte.rang
        return score

    def tour(self, carte_jouees):
        self.jouer()
        carte_jouees.append(self, self.carte_actuelle)

    @property
    def Deck(self):
        return self.deck

    @property
    def Defausse(self):
        return self.defausse

    @property
    def doitJouer(self):
        return self.carte_actuelle is None

    @property
    def CarteActuelle(self):
        return self.carte_actuelle

    @CarteActuelle.setter
    def CarteActuelle(self, value):
        self.carte_actuelle = value

    @property
    def Index(self):
        return self.index

    @Index.setter
    def Index(self, value):
        self.index = value
