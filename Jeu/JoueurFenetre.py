import time
from Reseau.Communicator import Communicator

from Jeu.DecorateurJoueur import DecorateurJoueur
from Jeu.JoueurBataille import JoueurBataille


class JoueurFenetre(DecorateurJoueur):
    def __init__(self, joueur):
        super().__init__(joueur)

    def jouer(self):
        while self.doitJouer:
            time.sleep(0.1)


