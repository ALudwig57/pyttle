from Jeu.JoueurBataille import JoueurBataille


class JoueurIA(JoueurBataille):
    def __init__(self, index):
        super().__init__(index=index, pseudo="IA{}".format(index))

    def jouer(self):
        self.pioche()
