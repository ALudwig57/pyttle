import abc
from Reseau.ChainOfResponsability import ChainOfResponsability


class CORReseau(ChainOfResponsability):

    def __init__(self, communicator, successor=None):
        super().__init__(successor)
        self._successor = successor
        self.communicator = communicator

    @abc.abstractmethod
    def request(self, request):
        pass
