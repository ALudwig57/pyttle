import abc


class ChainOfResponsability(metaclass=abc.ABCMeta):

    def __init__(self, successor=None):
        self._successor = successor

    @abc.abstractmethod
    def request(self, request):
        pass
