from Jeu.JoueurBataille import JoueurBataille
from Jeu.JoueurFenetre import JoueurFenetre
from Reseau.Communicator import Communicator
from Reseau.MaillonChercheurDeck import MaillonChercheurDeck
from Reseau.MaillonChercheurPartie import MaillonChercheurPartie
from Reseau.MaillonChercheurId import MaillonChercheurId
from Reseau.MaillonChercheurStart import MaillonChercheurStart
from Reseau.MaillonTourPioche import MaillonTourPioche


class ChercheurPartie(Communicator):

    def __init__(self, dialog, pseudo):
        super().__init__(dialog=dialog, joueur=JoueurFenetre(JoueurBataille(index=0, pseudo=pseudo)))
        self.tokensIndesirables = set()
        self.allowToSend = False
        self.partie = None
        self.patternEnvoi = None
        MaillonDeck = MaillonChercheurDeck(communicator=self, successor=None)
        MaillonStart = MaillonChercheurStart(communicator=self, successor=MaillonDeck)
        MaillonID = MaillonChercheurId(communicator=self, successor=MaillonStart)
        MaillonChercheur = MaillonChercheurPartie(communicator=self, successor=MaillonID)
        self.COR = MaillonTourPioche(communicator=self, successor=MaillonChercheur)
