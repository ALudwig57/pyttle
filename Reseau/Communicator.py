

# !/usr/bin/env python
"""An ivyprobe script for ivy-python
"""
import os, time
from threading import Thread
import abc

from ivy import std_api
from ivy.std_api import *

from Jeu.DecorateurJoueur import DecorateurJoueur

try:
    import readline
except ImportError:
    pass


class Communicator(Thread, DecorateurJoueur):
    __metaclass__ = abc.ABCMeta
    IVYAPPNAME = 'Pyttle'

    def __init__(self, dialog, joueur):
        Thread.__init__(self)
        DecorateurJoueur.__init__(self, joueur)
        self.ivybus = ''
        #self.readymsg = '[%s es prêt]' % self.IVYAPPNAME
        self.readymsg = ''
        self.COR = None
        self.verbose = 0
        self.showbind = 0
        self.tokenPartie = None
        self.__patternEnvoi = ''
        self.id = 0
        self.allowToSend = True
        self.dialog = dialog
        # initialising the bus
        IvyInit(self.IVYAPPNAME,  # application name for Reseau
                self.readymsg,  # ready message
                0,  # parameter ignored
                self.on_connection_change,  # handler called on connection/deconnection
                self.on_die  # handler called when a die msg is received
                )
        IvyBindMsg(self.on_msg, "(.*)")

    def info(self, fmt, *arg):
        print(fmt % arg)

    def on_connection_change(self, agent, event):
        if event == IvyApplicationDisconnected:
            self.info('L application Ivy %r a été déconnectée', agent)
        else:
            self.info('L application Reseau %r a été connectée', agent)
        self.info('Applications actuellement sur le bus: %s',
                  ','.join(IvyGetApplicationList()))

    def on_die(self, agent, id):
        self.info('Recu l ordre de mourrir depuis %r avec id = %d', agent, id)
        IvyStop()

    def on_msg(self, agent, *arg):
        print(arg and str(arg))
        self.COR.request(request=str(arg[0]).split('|'))

    def on_direct_msg(self, agent, num_id, msg):
        self.info('%r sent a direct message, id=%s, message=%s',
                  agent, num_id, msg)

    def on_regexp_change(self, agent, event, regexp_id, regexp):
        from ivy.ivy import IvyRegexpAdded
        self.info('%r %s regexp id=%s: %s',
                  agent, event == IvyRegexpAdded and 'added' or 'removed',
                  regexp_id, regexp)

    # if __name__ == '__main__':
    def sendToAll(self, msg):
        self.info('Envoyé à %s utilisateurs' % IvySendMsg(msg))

    @property
    def patternEnvoi(self):
        return self.__patternEnvoi

    @patternEnvoi.setter
    def patternEnvoi(self, pattern):
        self.__patternEnvoi = pattern

    def clearPattern(self):
        for (id, regexp) in std_api._IvyServer.get_subscriptions():
            IvyUnBindMsg(id)

    def send_msg(self, msg):
        IvySendMsg(msg)

    def run(self):
            from ivy.ivy import ivylogger, IvyApplicationDisconnected, void_function
            import logging

            ivylogger.setLevel(logging.WARN)

            self.info('Broadcasting on %s',
                      self.ivybus or os.environ.get('IVYBUS') or 'ivydefault')

            # starting the bus
            IvyStart(self.ivybus)

            # direct msg
            IvyBindDirectMsg(self.on_direct_msg)

            # Ok, time to go
            time.sleep(0.5)
            self.info('Connexion établie, prêt à recevoir des instructions')
            while 1:
                time.sleep(2)
                if self.allowToSend:
                    self.send_msg(self.patternEnvoi)
                    if self.patternEnvoi.startswith('PIOCHE'):
                        self.allowToSend = False

    def jouer(self):
        while self.doitJouer:
            time.sleep(.1)
        self.patternEnvoi = "PIOCHE|" + self.tokenPartie + "|" + str(
            self.joueur.index) + "|" + str(self.CarteActuelle.symbole) + "|" + str(self.CarteActuelle.rang)
        self.allowToSend = True

    @property
    def Index(self):
        return self.joueur.index

    @Index.setter
    def Index(self, value):
        self.joueur.index = value
