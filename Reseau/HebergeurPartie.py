from Jeu.JoueurBataille import JoueurBataille
from Jeu.JoueurFenetre import JoueurFenetre
from Jeu.JoueurIA import JoueurIA
from Reseau.Communicator import Communicator
from Reseau.MaillonHebergeurChercheur import MaillonHebergeurChercheur
from Reseau.MaillonHebergeurStart import MaillonHebergeurStart
from Reseau.MaillonHebergeurThank import MaillonHebergeurThank
from Reseau.MaillonTourPioche import MaillonTourPioche
from Bataille.BatailleLongue import BatailleLongue


class HebergeurPartie(Communicator):
    import secrets

    def __init__(self, nbjoueurs, dialog, pseudo, nbIA):
        super().__init__(dialog, JoueurFenetre(JoueurBataille(1, pseudo=pseudo)))
        self.tokenPartie = self.secrets.token_hex(4)
        self.idjoueurs = 2
        self.nbjoueurs = nbjoueurs
        self.nbIA = nbIA
        self.dialog.top.master.partie.joueurs.append(self)
        self.dialog.top.master.partie.indexjoueurfenetre = self.joueur.index
        if self.nbjoueurs == 0:
            self.patternEnvoi = ""
            for i in range(self.nbIA):
                self.dialog.top.master.partie.joueurs.append(JoueurIA(self.idjoueurs))
                self.idjoueurs += 1
            self.dialog.top.master.partie.distribue()
            self.start()
            self.dialog.top.master.show()
            self.dialog.top.destroy()
            if not self.dialog.top.master.partieController.is_alive():
                print("Partie lancée")
                self.dialog.top.master.partieController.start()
        else:
            self.patternEnvoi = "Heberge|" + self.tokenPartie
            self.checkdeckjoueurs = list()
            for i in range(0, self.nbjoueurs):
                self.checkdeckjoueurs.append(False)

            MaillonHebergeurPartie = MaillonHebergeurStart(communicator=self, successor=None)
            MaillonThank = MaillonHebergeurThank(communicator=self, successor=MaillonHebergeurPartie)
            MaillonChercheur = MaillonHebergeurChercheur(communicator=self, successor=MaillonThank)
            self.COR = MaillonTourPioche(communicator=self, successor=MaillonChercheur)

    def tramedeck(self):
        trametexte = "DECK|" + self.tokenPartie

        self.dialog.top.master.partie.distribue()
        if isinstance(self.dialog.top.master.partie, BatailleLongue):
            trametexte += "|"+"Longue"
        else:
            trametexte += "|"+"Courte"
        trametexte += "|" + str(self.dialog.top.master.partie.cartecachee)
        for joueur in self.dialog.top.master.partie.joueurs:
            trametexte += "|"
            trametexte += str(joueur.index)+":"+joueur.pseudo +":"
            for carte in joueur.Deck.pile:
                trametexte += str(carte.symbole) + "," + str(carte.rang) + ";"
            trametexte = trametexte[:-1]
        return trametexte
