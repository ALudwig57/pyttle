#!/usr/bin/env python
"""An ivyprobe script for ivy-python
"""
import os, string, sys, time, getopt
from threading import Thread

from ivy import std_api
from ivy.std_api import *

try:
    import readline
except ImportError:
    pass


class IvyConnect(Thread):
    IVYAPPNAME = 'Pyttle'

    def __init__(self):
        Thread.__init__(self)
        self.ivybus = ''
        self.readymsg = '[%s es prêt]' % self.IVYAPPNAME
        self.verbose = 0
        self.showbind = 0
        self.pattern = ''
        self.tokenPartie = None
        # initialising the bus
        IvyInit(self.IVYAPPNAME,  # application name for Reseau
                self.readymsg,  # ready message
                0,  # parameter ignored
                self.on_connection_change,  # handler called on connection/deconnection
                self.on_die  # handler called when a die msg is received
                )


    def info(self, fmt, *arg):
        print(fmt % arg)

    def on_connection_change(self, agent, event):
        if event == IvyApplicationDisconnected:
            self.info('L application Reseau %r a été déconnectée', agent)
        else:
            self.info('L application Reseau %r a été connectée', agent)
        self.info('Applications actuellement sur le bus: %s',
                  ','.join(IvyGetApplicationList()))

    def on_die(self, agent, id):
        self.info('Recu l ordre de mourrir depuis %r avec id = %d', agent, id)
        IvyStop()

    def on_msg(self, agent, *arg):
        self.info('Recu de %r: %s', agent, arg and str(arg) or '<no args>')

    def on_direct_msg(self, agent, num_id, msg):
        self.info('%r sent a direct message, id=%s, message=%s',
                  agent, num_id, msg)

    def on_regexp_change(self, agent, event, regexp_id, regexp):
        from ivy.ivy import IvyRegexpAdded
        self.info('%r %s regexp id=%s: %s',
                  agent, event == IvyRegexpAdded and 'added' or 'removed',
                  regexp_id, regexp)

    # if __name__ == '__main__':
    def sendToAll(self, msg):
        self.info('Envoyé à %s utilisateurs' % IvySendMsg(msg))

    def setPattern(self, pattern):
        self.clearPattern()
        self.pattern = pattern
        IvyBindMsg(self.on_msg, pattern)

    def hebergePartie(self, token):
        self.tokenPartie = token
        self.setPattern("^recrute_"+self.tokenPartie)

    def clearPattern(self):
        for (id, regexp) in std_api._IvyServer.get_subscriptions():
            IvyUnBindMsg(id)


    def run(self):
        from ivy.ivy import ivylogger, IvyApplicationDisconnected, void_function
        import logging

        ivylogger.setLevel(logging.WARN)

        self.info('Broadcasting on %s',
                  self.ivybus or os.environ.get('IVYBUS') or 'ivydefault')


        # starting the bus
        IvyStart(self.ivybus)

        # direct msg
        IvyBindDirectMsg(self.on_direct_msg)

        # Ok, time to go
        time.sleep(0.5)
        self.info('Connexion établie, prêt à recevoir des instructions')
        while 1:
            time.sleep(2)
            self.sendToAll("Coucou")
        #    try:
        #        msg = input('')
        #    except (EOFError, KeyboardInterrupt):
        #        msg = '.quit'
        #    if msg == '.quit':
        #        IvyStop()
        #        break
        #    else:
        #        self.info('Envoyé à %s utilisateurs' % IvySendMsg(msg))


ivy1 = IvyConnect()
ivy1.setPattern("Coucou")
ivy1.start()
ivy1.join()
