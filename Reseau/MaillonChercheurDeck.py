from Bataille.BatailleCourte import BatailleCourte
from Bataille.BatailleLongue import BatailleLongue
from Deck.Carte import Carte
from Jeu.JoueurBataille import JoueurBataille
from Jeu.JoueurIA import JoueurIA
from Reseau.CORReseau import CORReseau
import time


class MaillonChercheurDeck(CORReseau):
    def request(self, request):
        if request[1] != self.communicator.tokenPartie:
            return
        elif request[0] != "DECK":
            if self._successor is not None:
                self._successor.request(request)
        else:

            #Joueurs
            listJoueurs = [x for i, x in enumerate(request) if i > 3]
            #Type de partie (longue ou courte) + carte cachée ou non + indexJoueurFenetre dans la partie
            if request[2] == "Longue":
                self.communicator.dialog.top.master.partie = BatailleLongue(log=self.communicator.dialog.top.master.partieController.log, plateau=self.communicator.dialog.top.master.partieController.plateau, joueurs=[], cartecachee=bool(request[3]))
            else:
                self.communicator.dialog.top.master.partie = BatailleCourte(log=self.communicator.dialog.top.master.partieController.log, plateau=self.communicator.dialog.top.master.partieController.plateau, joueurs=[], cartecachee=bool(request[3]))
            self.communicator.dialog.top.master.partie.indexjoueurfenetre = self.communicator.Index

            for joueur in listJoueurs:
                player = None
                joueurSplite = str.split(joueur, ':')
                listcartes = joueurSplite[2].split(";")
                l2 = []
                for carte in listcartes:
                    infoscarte = carte.split(",")
                    l2.append(Carte(int(infoscarte[0]), int(infoscarte[1])))
                if self.communicator.Index == int(joueurSplite[0]):
                    player = self.communicator
                elif joueurSplite[1].startswith("IA"):
                    player = JoueurIA(int(joueurSplite[0]))
                else:
                    player = JoueurBataille(int(joueurSplite[0]), joueurSplite[1])
                player.ajout_deck(l2)
                self.communicator.dialog.top.master.partie.joueurs.append(player)
            self.communicator.patternEnvoi = "DECKTHANK|"+self.communicator.tokenPartie+"|"+str(self.communicator.Index)
            self.communicator.allowToSend = True
            time.sleep(2)
            self.communicator.allowToSend = False

