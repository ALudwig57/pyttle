from Reseau.CORReseau import CORReseau
import time

"""
Correspond à la deuxième étape de création de partie côté 
chercheur de partie : 
- Si le token qui est à l'interieur ne correspond pas au token de la partie, on arrête la chaîne
- Sinon
    - Si la requete commence par OK, on enregistre la quatrième partie de la requête comme un id personnel pour la partie
    - Si non, on passe au maillon suivant
"""


class MaillonChercheurId(CORReseau):
    def request(self, request):
        if request[1] != self.communicator.tokenPartie:
            return
        else:
            if request[0] == "OK":
                if request[2] == self.communicator.pseudo:
                    if self.communicator.Index == 0:
                        self.communicator.Index = int(request[3])
                        self.communicator.patternEnvoi = "Thanks|" + self.communicator.tokenPartie + '|' + str(
                            self.communicator.Index)
                        time.sleep(2)
                        self.communicator.allowToSend = False

                        self.communicator.dialog.question = "En attente de finalisation de partie"
            else:
                if self._successor is not None:
                    self._successor.request(request)
