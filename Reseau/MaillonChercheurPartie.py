from Reseau.CORReseau import CORReseau
from tkinter import messagebox

"""
Correspond à la première étape de création de partie côté 
chercheur de partie : 
- Si le token qui est à l'interieur est indésirable, on arrête la chaine
- Si le token n'est pas indésirable, on demande si on veut rejoindre la partie.
    - Si oui, le self.joueur est en attente de début de partie
    - Si non, le token est considéré comme indésirable
"""


class MaillonChercheurPartie(CORReseau):

    def request(self, request):
        # Si le chercheur a déjà un token
        if self.communicator.tokenPartie is not None:
            if self._successor is not None:
                self._successor.request(request)
        else:
            if request[0] == "Heberge":
                # Si le token est indésirable
                if request[1] in self.communicator.tokensIndesirables:
                    pass
                reponse = messagebox.askyesno("Rejoindre la partie ?", "Rejoindre la partie au token "+ request[1]+"?")
                if not reponse:
                    self.communicator.tokensIndesirables.add(request[1])
                else:
                    self.communicator.tokenPartie = request[1]
                    self.communicator.patternEnvoi = "Rejoindre|"+request[1]+"|"+self.communicator.pseudo
                    self.communicator.allowToSend = True
            pass
