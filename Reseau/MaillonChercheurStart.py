from Bataille.BatailleLongue import BatailleLongue
from Bataille.BatailleCourte import BatailleCourte
from Jeu.JoueurBataille import JoueurBataille
from Reseau.CORReseau import CORReseau
from Jeu.JoueurFenetre import JoueurFenetre

class MaillonChercheurStart(CORReseau):
    def request(self, request):
        if request[1] != self.communicator.tokenPartie:
            return
        else:
            if request[0] != 'START':
                if self._successor is not None:
                    self._successor.request(request)
            else:
                self.communicator.dialog.top.master.show()
                self.communicator.dialog.top.destroy()
                if not self.communicator.dialog.top.master.partieController.is_alive():
                    print("Partie lancée")
                    self.communicator.dialog.top.master.partieController.start()
                    self.communicator.allowToSend = False

