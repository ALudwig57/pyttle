from Reseau.CORReseau import CORReseau
from Jeu.JoueurBataille import JoueurBataille

"""
L'hébergeur reçoit une réponse favorable de la part d'un chercheur de partie
On va s'assurer que la requête contient le token de la partie, puis on va ajouter le self.joueur dans la liste
des self.joueurs de la partie.
On envoie l'id au joueur, jusqu'à ce qu'il emette un accusé de réception
"""


class MaillonHebergeurChercheur(CORReseau):

    def request(self, request):
        if request[0] == "Rejoindre" and request[1] == self.communicator.tokenPartie:
            if self.communicator.nbjoueurs > len(self.communicator.dialog.top.master.partie.joueurs)-1:
                self.communicator.dialog.top.master.partie.joueurs.append(JoueurBataille(self.communicator.idjoueurs, request[2]))
                self.communicator.patternEnvoi ="OK|"+self.communicator.tokenPartie+"|"+request[2]+'|'+str(self.communicator.idjoueurs)
                self.communicator.idjoueurs +=1
                self.communicator.dialog.updatenbjoueurs()
            else:
                pass

        else:
            if self._successor is not None:
                self._successor.request(request)
