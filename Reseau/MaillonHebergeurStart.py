import time
from Reseau.CORReseau import CORReseau
from Bataille.BatailleLongue import BatailleLongue
from Bataille.BatailleCourte import BatailleCourte


class MaillonHebergeurStart(CORReseau):
    def request(self, request):
        if request[1] != self.communicator.tokenPartie:
            return
        elif request[0] != "DECKTHANK":
            if self._successor is not None:
                self._successor.request(request)
        else:
            if not self.communicator.checkdeckjoueurs[int(request[2]) - 2]:
                self.communicator.checkdeckjoueurs[int(request[2])-2] = True
                for i in range(0, self.communicator.nbjoueurs):
                    if not self.communicator.checkdeckjoueurs[i]:
                        return
                pattern = "START|"+self.communicator.tokenPartie
                self.communicator.patternEnvoi = pattern
                self.communicator.dialog.top.master.show()
                self.communicator.dialog.top.destroy()
                if not self.communicator.dialog.top.master.partieController.is_alive():
                    print("Partie lancée")
                    self.communicator.dialog.top.master.partieController.start()
                    time.sleep(2)
                    self.communicator.allowToSend = False





