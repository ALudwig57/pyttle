from Jeu.JoueurIA import JoueurIA
from Reseau.CORReseau import CORReseau

"""

L'hébergeur a reçu un accusé de réception -> Il n'a plus besoin d'envoyer l'id au joueur.
S'il doit encore rechercher un ou des joueurs, on réinitialise le pattern de base à envoyer
Sinon, on créé une partie
"""


class MaillonHebergeurThank(CORReseau):

    def request(self, request):
        if request[1] != self.communicator.tokenPartie:
            return
        elif request[0] != "Thanks":
                if self._successor is not None:
                    self._successor.request(request)
        elif len(self.communicator.dialog.top.master.partie.joueurs)-1 < self.communicator.nbjoueurs:
                self.communicator.patternEnvoi = "Heberge|"+self.communicator.tokenPartie
                self.communicator.idjoueurs = self.communicator.idjoueurs + 1
        else:
            #pattern = "START|"+self.communicator.tokenPartie
            #for joueurs in self.communicator.partie.joueurs:
            #    pattern = pattern + "|" + "(" + joueurs.index +","+ joueurs.pseudo +")"
           # self.communicator.patternEnvoi = pattern
            for i in range(self.communicator.nbIA):
                self.communicator.dialog.top.master.partie.joueurs.append(JoueurIA(self.communicator.idjoueurs))
                self.communicator.idjoueurs += 1

            self.communicator.patternEnvoi = self.communicator.tramedeck()


