from Reseau.CORReseau import CORReseau
from Deck.Carte import Carte


class MaillonTourPioche(CORReseau):

    def request(self, request):
        if self.communicator.tokenPartie != request[1]:
            if self.communicator.tokenPartie is not None:
                return
            else:
                pass
        if request[0] != "PIOCHE":
            if self._successor is not None:
                self._successor.request(request)
        else:
            carte = Carte(symbole=int(request[3]), rang=int(request[4]))
            self.communicator.dialog.top.master.partie.getJoueurByIndex(int(request[2])).CarteActuelle = carte
