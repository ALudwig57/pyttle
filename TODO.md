Jeu de base :
- [x] Phase de bataille
- [ ] Vérifier les éventuels bugs
- [x] Assurer la connexion MVC
- [ ] Bataille à corriger

Variantes du jeu:
- [x] Version actuelle avec face cachée
- [ ] Version actuelle sans face cachée
- [x] Version courte avec face cachée
- [ ] Version courte sans face cachée

Ivy:
- [x] Héberger une partie
- [x] Rejoindre une partie
- [ ] Commencer une partie quand tous les joueurs sont là
- [ ] Quand un joueur joue une carte, informe les autres joueurs

TKinter:
- [x] Afficher le plateau
- [x] Le joueur peut prendre une carte et la mettre sur son tas
- [ ] Le joueur gagnant prend les cartes et les met sur son tas
- [ ] Interface Hebergement/recherche de partie



