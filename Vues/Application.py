import os
from tkinter import *
from tkinter import messagebox

from Controllers.PartieController import PartieController
from Vues.DialogChoix import DialogChoix
from Vues.DialogLabel import DialogLabel


class Application(Tk):
    def __init__(self, nom, hauteur, largeur):
        super().__init__()
        menubar = Menu(self)

        menu1 = Menu(menubar, tearoff=0)
        menu1.add_command(label="Nouvelle Partie", command=self.newgame)
        menu1.add_command(label="Quitter", command=self.stop)
        menubar.add_cascade(label="Fichier", menu=menu1)
        menubar.add_command(label="A propos", command=self.about)

        self.config(menu=menubar)

        self.minsize(hauteur, largeur)
        self.title(nom)

        self.protocol("WM_DELETE_WINDOW", self.stop)

        self.partie = None

        self.partieController = None

    def hide(self):
        self.withdraw()

    def show(self):
        self.config(height=800)
        self.deiconify()

    def stop(self):
        os.system('kill $PPID')

    def about(self):
        DialogLabel(self, "A propos", "Pyttle\nCrée par Alexandre Ludwig\net Arnaud Couderc\n2019").top.geometry('%dx%d' % (305, 70))
        # messagebox.showinfo("A propos", "Pyttle\nCrée par Alexandre Ludwig\net Arnaud Couderc\n2019")

    def newgame(self):
        DialogChoix(self)
        self.hide()

