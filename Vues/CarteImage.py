from PIL import ImageTk, Image

CHEMIN = "./ressources/cartes/"
PNG = ".png"
NONE = "None"
BACK = "back"


class CarteImage:
    def __init__(self, parent, newimage):
        self.parent = parent
        im = Image.open(CHEMIN + newimage + PNG)
        im = im.resize((100, 144), Image.ANTIALIAS)
        self.photo = ImageTk.PhotoImage(im)
        self.img = None
        self.image = newimage
        self.x = None
        self.y = None

    def create_image(self, x, y, anchor='nw'):
        self.img = self.parent.create_image(x, y, image=self.photo, anchor=anchor)

    def set_position(self, newx, newy):
        self.x = newx
        self.y = newy

    def move_image(self, event):
        self.x, self.y = event.x-50, event.y-72
        # self.parent.delete(self.img)
        # self.update_image_on_square(event, image)
        self.update_image_on_square(event)
        self.create_image(self.x, self.y)
        # self.parent.update()

    def place_image(self, event):
        self.x, self.y = event.x, event.y
        self.update_image(NONE)
        self.update_image(self.image)
        self.create_image(275, 350)

    def update_image_on_square(self, event):
        x, y = event.x, event.y
        if 375 > x > 275 and y >= 350:
            self.update_image(self.image)
        else:
            self.update_image(BACK)

    '''Rotation de l'image (Marche pas)'''
    def rotate_image(self):
        im = Image.open(CHEMIN + self.image + PNG)
        im = im.resize((100, 144), Image.ANTIALIAS)
        im.transpose(Image.ROTATE_180)
        self.photo = ImageTk.PhotoImage(im)

    def update_image(self, newimage):
        self.image = newimage
        im = Image.open(CHEMIN + newimage + PNG)
        im = im.resize((100, 144), Image.ANTIALIAS)
        self.photo = ImageTk.PhotoImage(im)


