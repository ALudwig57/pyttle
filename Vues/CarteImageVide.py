from Vues.CarteImage import CarteImage

NONE = "None"


class CarteImageVide(CarteImage):
    def __init__(self, parent):
        super().__init__(parent, NONE)
