from tkinter import *
from Vues.DialogHebergement import DialogHebergement
from Vues.DialogRecherchePartie import DialogRecherchePartie

'''Boite de dialogue choix partie'''


class DialogChoix:
    def __init__(self, parent):
        self.parent = parent
        self.top = Toplevel(parent)
        self.top.title("Choix de la partie")
        self.labelpseudo = Label(self.top, text="Pseudo : ")
        self.labelpseudo.grid(row=0, column=0, columnspan=1)
        self.pseudo = Entry(self.top)
        self.pseudo.grid(row=0, column=1, columnspan=1)
        self.question = Label(self.top, text="Souhaitez-vous héberger ou rejoindre une partie ?")
        self.question.grid(row=1, column=0, columnspan=2)
        self.b_heberger = Button(self.top, text="Héberger", command=lambda:self.heberge(self.parent))
        self.b_rejoindre = Button(self.top, text="Rejoindre", command=lambda:self.rejoindre(self.parent))
        self.b_heberger.grid(row=2, column=0)
        self.b_rejoindre.grid(row=2, column=1)

        ws = self.top.winfo_screenwidth()
        hs = self.top.winfo_screenheight()

        x = (ws / 2) - (315 / 2)
        y = (hs / 2) - (80 / 2)

        self.top.geometry('%dx%d+%d+%d' % (305, 80, x, y))

    def rejoindre(self, parent):
        DialogRecherchePartie(parent=parent, pseudo=self.pseudo.get())
        self.top.destroy()

    def heberge(self, parent):
        DialogHebergement(parent=parent, pseudo=self.pseudo.get())
        self.top.destroy()


