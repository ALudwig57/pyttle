from tkinter import *


class DialogFin:
    def __init__(self, parent):
        self.top = Toplevel(parent)
        self.top.title("Partie Terminée")
        self.question = Label(self.top, text="Partie Terminée")
        self.question.grid(row=0, column=0, columnspan=2)
        self.rejouer, self.quitter = Button(self.top, text="Rejouer", command=self.ok), Button(self.top,
                                                                                               text="Quitter",
                                                                                               command=self.ok)
        self.rejouer.grid(row=1, column=0)
        self.quitter.grid(row=1, column=1)

        ws = self.top.winfo_screenwidth()
        hs = self.top.winfo_screenheight()

        x = (ws / 2) - (310 / 2)
        y = (hs / 2) - (50 / 2)
        self.top.geometry('%dx%d+%d+%d' % (310, 50, x, y))

    def ok(self):
        self.top.destroy()


root = Tk()
d = DialogFin(root)
root.mainloop()
