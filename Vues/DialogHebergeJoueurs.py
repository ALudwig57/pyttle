from Vues.DialogLabel import DialogLabel


class DialogHebergeJoueurs(DialogLabel):
    def __init__(self, parent, nbJoueurs, nbia, pseudo):
        super().__init__(parent, "Recherche de joueurs", "Nombre de joueurs trouvés: 0/"+str(nbJoueurs))
        from Reseau.HebergeurPartie import HebergeurPartie
        self.Hebergeur = HebergeurPartie(nbjoueurs=nbJoueurs, nbIA=nbia, dialog=self, pseudo=pseudo)
        self.updatenbjoueurs()
        if nbJoueurs > 0 :
            self.Hebergeur.start()

    def updatenbjoueurs(self):
        self.setLabelDialog("Nombre de joueurs trouvés à la partie {} : \n {}/{}".format(self.Hebergeur.tokenPartie, len(self.Hebergeur.dialog.top.master.partie.joueurs)-1, self.Hebergeur.nbjoueurs))

