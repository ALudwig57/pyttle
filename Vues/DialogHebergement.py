import os
from tkinter import *
from tkinter import messagebox
from Bataille.Partie import Partie

from Bataille.BatailleCourte import BatailleCourte
from Bataille.BatailleLongue import BatailleLongue
from Jeu.JoueurFenetre import JoueurFenetre
from Jeu.JoueurBataille import JoueurBataille
from Reseau.Communicator import Communicator
from Reseau.HebergeurPartie import HebergeurPartie
from Reseau.ChercheurPartie import ChercheurPartie
from Vues.Logger import Logger
from Vues.DialogHebergeJoueurs import DialogHebergeJoueurs

'''Boite de dialogue d'hébergement de partie'''


class DialogHebergement:
    def __init__(self, parent, pseudo=None):
        self.top = Toplevel(parent)
        self.parent = parent
        self.top.title("Paramètres de la partie")
        self.pseudo = pseudo
        self.logger = Logger(self.top.master)

        self.nom_partie_txt = Label(self.top, text="Nom de la partie :")

        self.nb_joueurs_txt = Label(self.top, text="Nb joueurs :", justify="right")
        self.nb_ia_txt = Label(self.top, text="Nb IA :", justify="right")
        self.version_txt = Label(self.top, text="Version :")

        self.nom_partie = Entry(self.top)
        self.nom_partie.insert(END, "Pyttle")

        self.nb_joueurs = Entry(self.top)
        self.nb_joueurs.insert(END, "0")
        self.nb_ia = Entry(self.top)
        self.nb_ia.insert(END, "1")
        self.var_hiddencard = IntVar()
        self.carte_a_envers = Checkbutton(self.top, text="Cartes à l'envers", variable=self.var_hiddencard)
        self.carte_a_envers.select()

        self.version = IntVar()
        self.version.set(1)

        self.version_actuelle = Radiobutton(self.top, variable=self.version, text="Actuelle", value=1, indicatoron=0)
        self.version_courte = Radiobutton(self.top, variable=self.version, text="Courte", value=2, indicatoron=0)

        self.nom_partie_txt.grid(row=0, column=0, columnspan=2)
        self.nom_partie.grid(row=0, column=1, columnspan=4)

        self.nb_joueurs_txt.grid(row=2, column=0)
        self.nb_joueurs.grid(row=2, column=1)
        self.nb_ia_txt.grid(row=4, column=0)
        self.nb_ia.grid(row=4, column=1)

        self.carte_a_envers.grid(row=2, column=2, columnspan=2)
        self.version_txt.grid(row=4, column=2)
        self.version_actuelle.grid(row=4, column=3)
        self.version_actuelle.select()
        self.version_courte.grid(row=4, column=4)

        self.annuler = Button(self.top, text="Annuler", command=lambda: self.kill)
        self.valider = Button(self.top, text="Valider", command=lambda: self.hebergement())

        self.annuler.grid(row=5, column=1)
        self.valider.grid(row=5, column=3)

        ws = self.top.winfo_screenwidth()
        hs = self.top.winfo_screenheight()

        x = (ws / 2) - (310 / 2)
        y = (hs / 2) - (100 / 2)
        self.top.geometry('%dx%d+%d+%d' % (450, 100, x, y))

    def WhichPartie(self, joueurs, log, plateau):

        boolhiddencard = self.var_hiddencard.get() == 1
        print("Version : " + str(self.version.get()))
        if self.version.get() == 2:
            return BatailleCourte(log, plateau, joueurs, boolhiddencard)
        elif self.version.get() == 1:
            return BatailleLongue(log, plateau, joueurs, boolhiddencard)

    def howManyPlayers(self):
        # nbJoueurs = input("Combien de joueurs pour cette partie ? (2 à 4)")
        try:
            valnbJoueurs = int(self.nb_joueurs.get())
            joueurs = [JoueurFenetre(JoueurBataille(1, "Moi"))]
            for i in range(2, valnbJoueurs + 1):
                # nom = input("Nom du joueur " + str(i) + " : ")
                nom = "IA "+str(i)
                joueurs.append(JoueurBataille(i, nom))
            return tuple(joueurs)
        except ValueError:
            print("Vous n'avez pas rentré un entier")

    def hebergement(self):
        nbjoueurs = int(self.nb_joueurs.get())
        nbia = int(self.nb_ia.get())

        if 1 <= nbjoueurs+nbia <= 3:
            self.top.master.partie = self.WhichPartie([], self.top.master.partieController.log, self.top.master.partieController.plateau)
            DialogHebergeJoueurs(parent=self.parent, nbJoueurs=nbjoueurs, nbia=nbia, pseudo=self.pseudo)
            self.top.destroy()
        else:
            messagebox.showwarning("Erreur", "Nombre de joueurs ou d'IA invalide")

    ''' Démarrage de la partie dans la fenetre principale'''
    def SendPartie(self):
        self.top.master.partieController.start()
        self.top.master.show()
        self.top.destroy()

    def kill(self):
        os.system('kill $PPID')

