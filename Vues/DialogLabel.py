from tkinter import Toplevel, Label, StringVar


class DialogLabel:
    def __init__(self, parent, title, label):
        self.parent = parent
        self.top = Toplevel(parent)
        self.__labeldialog = StringVar()
        self.__labeldialog.set(label)

        self.top.title(title)
        self.question = Label(self.top, textvariable=self.__labeldialog)
        self.question.pack()

        ws = self.top.winfo_screenwidth()
        hs = self.top.winfo_screenheight()

        x = (ws / 2) - (305 / 2)
        y = (hs / 2) - (50 / 2)
        self.top.geometry('%dx%d+%d+%d' % (305, 50, x, y))

    @property
    def labeldialog(self):
        return self.question['text']

    @labeldialog.setter
    def labeldialog(self, label):
        self.__labeldialog.set(label)

    def setLabelDialog(self, label):
        self.__labeldialog.set(label)
