from tkinter import *
from Reseau.ChercheurPartie import ChercheurPartie
from Vues.DialogLabel import DialogLabel

'''Boite de dialogue souhaitant rejoindre une partie'''


class DialogRecherchePartie(DialogLabel):
    def __init__(self, parent, pseudo=None):
        super().__init__(parent, "Recherche de partie", "Recherche de partie en cours")
        self.Chercheur = ChercheurPartie(pseudo=pseudo, dialog=self)
        self.Chercheur.start()

    def partieFound(self):
        self.setLabelDialog("En attente de finalisation de partie...")