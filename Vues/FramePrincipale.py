from tkinter import *
from Vues.Plateau import Plateau


def motionh (event):
    x, y = event.x, event.y
    print('{}, {}'.format(x, y))


class FramePrincipale(Frame):
    def __init__(self, master=None):
        super().__init__(master)

        self.plateau = Plateau(self, 800, 500)
        self.plateau.grid(row=0, column=0, rowspan=1, columnspan=4)

        self.plateau.bind("<B1-Motion>", lambda event: self.plateau.move_carte(event=event, index=1))
        self.plateau.bind("<ButtonRelease-1>", self.placement_carte)

        self.console = Text(self, width=114)
        self.console.grid(row=1, column=0, rowspan=1, columnspan=4)

    '''Gère la jouabilité avec déplacement depuis le deck'''
    def placement_carte(self, event):
        if self.plateau.isHoldingCard:
            self.plateau.place_carte_joueur(event=event, index=1)
        if 375 > event.x > 275 and event.y >= 350:
            self.master.partie.getJoueurFenetre().pioche()
