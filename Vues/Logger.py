import logging
import tkinter
from Jeu.JoueurBataille import JoueurBataille
from Bataille.BatailleCourte import BatailleCourte
from Bataille.BatailleLongue import BatailleLongue
from Observer.Observer import Observer

'''Gère le log dans la fenêtre'''


class Logger(logging.Handler, Observer):
    def maj(self, arg):
        if isinstance(arg, str):
            trame = arg.split('|')
            if len(trame) == 3:
                if trame[2] != "back":
                    self.emit(trame[1] + trame[2].replace('_', ' '))
            else:
                self.emit(arg.replace('|', ' ').replace('_', ' '))

        # trame = arg
        # trame = trame.replace('_', ' ')
        # trame = trame.replace('|', '')
        # self.emit(trame)

    def __init__(self, widget):
        logging.Handler.__init__(self)
        self.setLevel(logging.INFO)
        self.widget = widget
        # self.widget.config(state='disabled')

    def emit(self, record):
        #self.widget.config(state='normal')
        # Append message (record) to the widget
        self.widget.insert(tkinter.END, record + '\n')
        self.widget.see(tkinter.END)  # Scroll to the bottom
        #self.widget.config(state='disabled')

    def tour(self, joueur):
        self.emit("Au tour de " + joueur.pseudo)

    def afficheScore(self, joueur):
        self.emit("Score du joueur "+joueur.pseudo+" : "+str(joueur.score_defausse()))
