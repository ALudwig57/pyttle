from tkinter import *

from Observer.Observer import Observer
from Vues.CarteImageDos import CarteImageDos
from Vues.CarteImageVide import CarteImageVide

deck_j1 = 400, 350
deck_j2 = 0, 175
deck_j3 = 400, 0
deck_j4 = 700, 175

carte_j1 = 275, 350
carte_j2 = 125, 175
carte_j3 = 275, 0
carte_j4 = 575, 175

gagnant = 375, 175

joueurs = ((deck_j1, deck_j2, deck_j3, deck_j4), (carte_j1, carte_j2, carte_j3, carte_j4))


# Taille Carte : 100 x 144


class Plateau(Canvas, Observer):
    def maj(self, arg):
        if isinstance(arg, str):
            try:
                index, phrase, carte = arg.split("|")
            except ValueError:
                pass
            else:
                index = int(index)
                self.carte_joueurs[int(index)-1].update_image(carte)
                self._show_image_carte_joueur(index)

    @property
    def carte_joueur(self, index):
        return self.joueurs[1][index - 1]

    def __init__(self, master=None, width=800, height=500):
        Canvas.__init__(self, master, width=width, height=height)
        self._image_deck = CarteImageDos(self)
        # self.tas_gagnant = CarteImageDos(self)
        # self.tas_gagnant.rotate_image()
        self.carte_joueurs = []
        for i in range(0, 4):
            self.carte_joueurs.append(CarteImageVide(self))
        # self.carte_joueurs = [self._carte_joueur_1, self._carte_joueur_2, self._carte_joueur_3, self._carte_joueur_4]
        self.init_images_deck()
        self.init_images_cartes()
        self.isHoldingCard = False

    def _get_carte_joueur(self, index):
        return self.carte_joueurs[index - 1]

    def move_carte_joueur(self, event, index):
        self.isHoldingCard = True
        if carte_j1[0] <= event.x <= deck_j1[0] + 100 and deck_j1[1] <= event.y <= deck_j1[0] + 144:
            self.carte_joueurs[index - 1].move_image(event)

    def place_carte_joueur(self, event, index):
        self.isHoldingCard = False
        if 375 > event.x > 275 and event.y >= 350:
            self.carte_joueurs[index - 1].place_image(event=event)

    def init_images_deck(self):
        for i in range(0, 4):
            self._image_deck.create_image(joueurs[0][i][0], joueurs[0][i][1])
        # self.tas_gagnant.create_image(gagnant[0], gagnant[1])

    def init_images_cartes(self):
        for i in range(0, 4):
            self._show_image_carte_joueur(i)

    def _show_image_carte_joueur(self, index):
        self.carte_joueurs[index - 1].create_image(joueurs[1][index - 1][0], joueurs[1][index - 1][1])

    '''Mise a jour de l'image du tas gagnant (Pas encore utilisé)'''
    def update_tas_gagnant(self):
        if self.tas_gagnant.image == "None":
            self.tas_gagnant = CarteImageDos(self)
        else:
            self.tas_gagnant = CarteImageVide(self)

    '''Déplacement du tas gagnant'''
    def move_tas_gagnant(self, event):
        if gagnant[0] <= event.x <= gagnant[0] + 100 and gagnant[1] <= event.y <= gagnant[0] + 144:
            self.tas_gagnant.move_image(event)

    '''déplacement des cartes'''
    def move_carte(self, event, index):
        self.move_carte_joueur(event, index)
        # self.move_tas_gagnant(event)

