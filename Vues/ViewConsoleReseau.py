from Jeu.JoueurBataille import JoueurBataille
from Reseau.HebergeurPartie import HebergeurPartie
from Reseau.ChercheurPartie import ChercheurPartie
from Bataille.BatailleCourte import BatailleCourte
from Bataille.BatailleLongue import BatailleLongue


class ViewConsoleReseau:
    def print(x):
        print(x)

    def setTypeOfJoueur(self):
        reponse = ""
        while reponse != "1" and reponse != "2":
            reponse = input("Voulez vous héberger (1) ou rejoindre (2) une partie ?")
        if reponse == "1":
            return HebergeurPartie(self.howManyPlayers())
        else:
            return ChercheurPartie()

    def howManyPlayers(self):
        nbJoueurs = input("Combien de joueurs voulez vous rechercher ? (1 à 3)")
        try:
            valnbJoueurs = int(nbJoueurs)
            return valnbJoueurs
        except ValueError:
            print("Vous n'avez pas rentré un entier")

    def tour(self, joueur):
        print("Au tour de " + joueur.pseudo)

    def WhichPartie(self, *joueurs):
        reponse = ""
        while reponse != "1" and reponse != "2":
            reponse = input("Voulez vous jouer une partie courte (1) ou longue (2) ?")
        if reponse == "1":
            return BatailleCourte(*joueurs)
        else:
            return BatailleLongue(*joueurs)

    def afficheScore(self, joueur):
        print("Score du joueur " + joueur.pseudo + " : " + str(joueur.score_defausse()))
