from tkinter import Tk

from Controllers.PartieController import PartieController
from Vues.Application import Application
from Vues.DialogChoix import DialogChoix
from Vues.FramePrincipale import FramePrincipale
from Vues.Logger import Logger

App = Application("Pyttle", 800, 600)
FramePrincipale = FramePrincipale(App)
Logger = Logger(FramePrincipale.console)

FramePrincipale.pack()
Heberger = DialogChoix(App)
Heberger.top.grab_set()

App.partieController = PartieController(App, Logger, FramePrincipale.plateau)
App.lift()
App.hide()
App.mainloop()

